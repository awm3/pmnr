# Paid Monero Nostr Relays

Via this README file, it is possible to add a paid Monero Nostr relay via a pull request, which will later be listed under https//pmnr.xmr.rocks. The addition is done manually. http and wss will be take from given url.

- xmr.usenostr.org - 0.01 XMR
- nostr.portemonero.com - 0.02 XMR
- nostr.xmr.rocks - 0.02 XMR
- nerostr.xmr.rocks - 0.02 XMR
- xmr.ithurtswhenip.ee - 0.007 XMR
